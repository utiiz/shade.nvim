-- Main
local M = {}

M.setup = function(opts)
	vim.api.nvim_command([[
    augroup shade
    au!
    au WinEnter * call v:lua print(vim.inspect(opts))
    augroup END
  ]])
end

return M
